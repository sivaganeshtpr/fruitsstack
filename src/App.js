import './App.css';
import LoginPanel from './components/LoginPanel/LoginPanel'
import {Provider} from 'react-redux'
import store from './redux/AppStore'

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <header className="App-header">
          <LoginPanel />
        </header>
      </div>
    </Provider>
  );
}

export default App;
