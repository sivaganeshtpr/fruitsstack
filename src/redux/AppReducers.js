import {ADD_FRUIT, POP_FRUIT} from './ActionTypes'
import {fruits} from '../assets/data/fruits.json'

const initialStockState = {
    stock : [],
    fruits : fruits.map((elem, index)=> ({...elem, "count": 10, "index": index}))
}

const AppReducer = (state = initialStockState, action) => {
    switch(action.type) {
        case ADD_FRUIT : 
            let currentFruit = {...state.fruits[action.value]};
            if(currentFruit.count > 0) {
                let newStock = [...state.stock, {name: currentFruit.name, color:currentFruit.color, index: currentFruit.index}]
                let newFruits = [...state.fruits];
                newFruits.splice(action.value, 1, {...currentFruit, count: currentFruit.count-1})
                
                return {
                    stock : newStock,
                    fruits : newFruits
                }   
            }
        
        break;
        case POP_FRUIT: 
            if(state.stock.length > 0) {
                let newStock = [...state.stock]
                let popFruit = newStock.pop();
                let currentFruit = {...state.fruits[popFruit.index]};
                let newFruits = [...state.fruits];
                newFruits.splice(popFruit.index, 1, {...currentFruit, count: currentFruit.count-1})

                return {
                    stock : newStock,
                    fruits : newFruits
                }   
            }
        
        break;
        default: return state;
    }
}

export default AppReducer