import {ADD_FRUIT, POP_FRUIT} from './ActionTypes'

export const addToStock = (value) => {
    return{
        type : ADD_FRUIT,
        value : value
    }

}
export const popFromStock = (value) => {
    return {
        type: POP_FRUIT,
        value: value
    }
}