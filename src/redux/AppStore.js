import {createStore} from 'redux'

import AppReducer from './AppReducers'

const store = createStore(AppReducer);

export default store;